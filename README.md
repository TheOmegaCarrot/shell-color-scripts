# Shell Color Scripts

![Screenshot of shell-color-scripts](https://gitlab.com/dwt1/dotfiles/raw/master/.screenshots/dotfiles12.png)

Forked by TheOmegaCarrot:

Changes:

colorscript -r no longer prints the name of script.

Added blank lines to the end of some scripts.

Modified some scripts to prevent them from automatically clearing the terminal

Removed the scripts which make the terminal unusable until the script is ended or halted.

Added install script


A collection of terminal color scripts accumulated by Derek Taylor over the years, modified by TheOmegaCarrot.
Included 48 beautiful terminal color scripts.

# Installing shell-color-scripts on other Linux distrtibutions

Download the source code from this repository or use a git clone:

	git clone https://gitlab.com/TheOmegaCarrot/shell-color-scripts
	cd shell-color-scripts
	sudo ./install

	Or, install manually:
	git clone https://gitlab.com/TheOmegaCarrot/shell-color-scripts
	cd shell-color-scripts
    rm -rf /opt/shell-color-scripts || return 1
    sudo mkdir -p /opt/shell-color-scripts/colorscripts || return 1
    sudo cp -rf colorscripts/* /opt/shell-color-scripts/colorscripts
    sudo cp colorscript.sh /usr/bin/colorscript

    # optional for zsh completion
    sudo cp zsh_completion/_colorscript /usr/share/zsh/site-functions

# Usage

    colorscript --help
    Description: A collection of terminal color scripts.

    Usage: colorscript [OPTION] [SCRIPT NAME/INDEX]
      -h, --help, help    	Print this help.
      -l, --list, list    	List all color scripts.
      -r, --random, random	Run a random color script.
      -e, --exec, exec    	Run a spesific color script by SCRIPT NAME or INDEX.


# The Scripts Are Located in /opt/shell-color-scripts/colorscripts

The source for shell-color-scripts is placed in:

	/opt/shell-color-scripts/colorscripts

For even more fun, add the following line to your .bashrc or .zshrc and you will run a random color script each time you open a terminal:

	### RANDOM COLOR SCRIPT ###
	colorscript random
